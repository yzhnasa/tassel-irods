package org.bio5.irods.iplugin.fileoperations;

import ij.IJ;
import ij.ImagePlus;
import ij.io.Opener;

import java.io.File;
import java.net.SocketTimeoutException;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import net.maizegenetics.analysis.data.FileLoadPlugin;
import net.maizegenetics.analysis.data.FileLoadPlugin.TasselFileType;
import net.maizegenetics.gui.DialogUtils;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginEvent;
import net.maizegenetics.prefs.TasselPrefs;
import net.maizegenetics.util.ExceptionUtils;
import net.maizegenetics.util.Utils;

import org.apache.log4j.Logger;
import org.bio5.irods.iplugin.bean.IPlugin;
import org.bio5.irods.iplugin.bean.TapasCoreFunctions;
import org.bio5.irods.iplugin.utilities.IrodsUtilities;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.exception.OverwriteException;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.transfer.TransferControlBlock;

public class GetFileFromIrodsSwingWorker extends SwingWorker<Void, Integer>
		implements Runnable {

	private IRODSFileFactory iRODSFileFactory;
	private String treePath;
	private DataTransferOperations dataTransferOperationsAO;
	private IPlugin iPlugin;
	private DataObjectAO dataObjectAO;
	private TransferControlBlock transferControlBlock;
	IRODSFile sourceIrodsFilePath = null;
	private int executeTimes = 0;

	/* Logger instantiation */
	static Logger log = Logger.getLogger(GetFileFromIrodsSwingWorker.class
			.getName());

	/* Get files from iRODS Server */
	public GetFileFromIrodsSwingWorker(IRODSFileFactory iRODSFileFactory,
			String treePath, IPlugin irodsImagej, JProgressBar progressbar) {
		this.iRODSFileFactory = iRODSFileFactory;
		this.treePath = treePath;
		this.iPlugin = irodsImagej;
	}

	/*
	 * Using SwingWorker-doInBackGround() function to do processing in
	 * background
	 */
	@Override
	public Void doInBackground() throws JargonException, SocketTimeoutException {

		log.info("finalTreePath:" + this.treePath);

		if (null != iPlugin.getCustomPath() && null != this.treePath) {
			String[] customPathTokens = IrodsUtilities
					.getStringTokensForGivenURI(this.treePath);
			String newtreePath = "";
			log.info("length of tokens: " + customPathTokens.length);

			/* Start from index2 coz first 2 values are already available */
			for (int i = 2; i < customPathTokens.length; i++) {
				newtreePath = newtreePath + IrodsUtilities.getPathSeperator()
						+ customPathTokens[i].toString();
			}
			log.info("Final internal loop path for custom path: " + newtreePath);
			newtreePath = IrodsUtilities
					.replaceBackSlashWithForwardSlash_ViceVersa(newtreePath);
			this.treePath = (this.iPlugin.getCustomPath() + newtreePath);
			log.info("final tree path: " + this.treePath);

		} else {
			log.error("either customPath or treePath is null");
		}

		if (null != iPlugin) {
			this.transferControlBlock = this.iPlugin.getTransferControlBlock();
			if (null != this.transferControlBlock) {
				this.iPlugin.setTransferOptions(this.transferControlBlock
						.getTransferOptions());
			} else {
				log.error("transferControlBlock is null");
			}
			// iPlugin.getTransferOptions().setMaxThreads(10);
			this.dataTransferOperationsAO = this.iPlugin.getIrodsFileSystem()
					.getIRODSAccessObjectFactory()
					.getDataTransferOperations(this.iPlugin.getIrodsAccount());
			/*
			 * Check if user requires all files under home directory - this has
			 * performance degradation.
			 */
			if (null != this.iPlugin.getCustomPath()) {
				this.sourceIrodsFilePath = this.iRODSFileFactory
						.instanceIRODSFile(this.treePath);
				log.info("sourceIrodsFilePath" + sourceIrodsFilePath);
			} else if ((this.iPlugin.isHomeDirectoryTheRootNode())
					&& (null == this.iPlugin.getCustomPath())) {
				this.sourceIrodsFilePath = this.iRODSFileFactory
						.instanceIRODSFile(TapasCoreFunctions
								.getRootDirectoryPath(this.iPlugin)
								+ this.treePath);

				log.info("sourceIrodsFilePath" + this.sourceIrodsFilePath);
			} else if ((!this.iPlugin.isHomeDirectoryTheRootNode())
					&& (null == this.iPlugin.getCustomPath())) {
				this.sourceIrodsFilePath = this.iRODSFileFactory
						.instanceIRODSFile(TapasCoreFunctions
								.getHomeDirectoryPath(this.iPlugin)
								+ this.treePath);

				log.info("sourceIrodsFilePath" + this.sourceIrodsFilePath);
			}

			/****************************** Zhong Yang ***********************************/
			iPlugin.setSourceIrodsFilePath(sourceIrodsFilePath);
			/*****************************************************************************/

			this.dataObjectAO = this.iPlugin.getIrodsFileSystem()
					.getIRODSAccessObjectFactory()
					.getDataObjectAO(this.iPlugin.getIrodsAccount());

			/* Getting MD5 checksum of the current file from iRODS */
			String md5ChecksumLocalFile = null;
			String md5ChecksumServerFile = null;
			try {
				md5ChecksumServerFile = this.dataObjectAO
						.computeMD5ChecksumOnDataObject(this.sourceIrodsFilePath);
			} catch (Exception e) {
				log.info("Error while reading MD5 checksum of md5ChecksumServerFile"
						+ e.getMessage());

				JOptionPane
						.showMessageDialog(
								null,
								"Error while reading MD5 checksum of md5ChecksumServerFile!",
								"Error", 0);
			}
			File destinationLocalFilePath = new File(
					this.iPlugin.getImageJCacheFolder());
			log.info("sourceIrodsFilePath before inserting file"
					+ this.sourceIrodsFilePath);
			log.info("destinationLocalFilePath before inserting file"
					+ destinationLocalFilePath);

			/* Getting MD5 checksum of local file, if exists */
			File localFile = new File(
					destinationLocalFilePath.getAbsolutePath()
							+ IrodsUtilities.getPathSeperator()
							+ this.sourceIrodsFilePath.getName());
			md5ChecksumLocalFile = IrodsUtilities
					.calculateMD5CheckSum(localFile);
			log.info("MD5checksum of iRODS server file: "
					+ md5ChecksumServerFile);
			log.info("MD5checksum of local file: " + md5ChecksumLocalFile);

			if ((null != md5ChecksumLocalFile)
					&& (null != md5ChecksumServerFile)
					&& ("" != md5ChecksumLocalFile)
					&& ("" != md5ChecksumServerFile)) {
				log.info("MD5 checksum compared - are they Similar files ?"
						+ md5ChecksumLocalFile.equals(md5ChecksumServerFile));

				if (!md5ChecksumLocalFile.equals(md5ChecksumServerFile))
					JOptionPane
							.showMessageDialog(
									null,
									"Local cache directory have files with same name but MD5 checksum is different!",
									"Information", 1);
				if (md5ChecksumLocalFile.equals(md5ChecksumServerFile)) {
					JOptionPane
							.showMessageDialog(
									null,
									"File already exists in local. MD5 checksum of local file and remote file is same!",
									"Information", 1);
				}
			}
			try {
				if ((null != this.sourceIrodsFilePath)
						&& (null != destinationLocalFilePath)) {
					if (null != this.iPlugin) {

						log.info("Defaulting ErrorWhileUsingGetOperation value to :False");
						this.iPlugin.setErrorWhileUsingGetOperation(false);

						log.info("Transfer Options in IntraFileStatusCallBack status: "
								+ this.transferControlBlock
										.getTransferOptions());
						this.dataTransferOperationsAO
								.getOperation(
										this.sourceIrodsFilePath,
										destinationLocalFilePath,
										this.iPlugin
												.getIrodsTransferStatusCallbackListener(),
										this.transferControlBlock);

						if (!this.iPlugin.isErrorWhileUsingGetOperation()) {
							log.info("Executing openImageUsingImageJ method");
							// openImageUsingImageJ();
							loadDataFileToTassel(executeTimes);
							executeTimes++;
						} else {
							log.error("Error while transferring files");
							JOptionPane.showMessageDialog(null,
									"Error while transfering files!", "Error",
									0);
						}
					}
				}
			} catch (OverwriteException overwriteException) {
				log.error("File with same name already exist in local directory! "
						+ overwriteException.getMessage());
				JOptionPane
						.showMessageDialog(
								null,
								"File with same name already exist in local directory!",
								"Information", 1);

				/* Getting MD5 checksum of local file, if exists */
				File fileInLocal = new File(
						destinationLocalFilePath.getAbsolutePath()
								+ IrodsUtilities.getPathSeperator()
								+ this.sourceIrodsFilePath.getName());
				md5ChecksumLocalFile = IrodsUtilities
						.calculateMD5CheckSum(fileInLocal);
				log.info("MD5checksum of local file: " + md5ChecksumLocalFile);

				log.info("MD5 checksum compared - Similar files:"
						+ md5ChecksumLocalFile.equals(md5ChecksumServerFile));

				if (!md5ChecksumLocalFile.equals(md5ChecksumServerFile))
					JOptionPane
							.showMessageDialog(null,
									"File names are same but MD5 checksum is different!");

				overwriteException.printStackTrace();
			} catch (DataNotFoundException dataNotFoundException) {
				JOptionPane.showMessageDialog(null, "dataNotFoundException!",
						"Error", 0);

				log.info("Error while pulling files!"
						+ dataNotFoundException.getMessage());
			} catch (JargonException jargonException) {
				JOptionPane.showMessageDialog(null,
						"Error while pulling files!", "Error", 0);

				log.info("Error while pulling files!"
						+ jargonException.getMessage());
			}

		}
		return null;
	}

	/******************************* Zhong Yang ************************************/
	// When finished download files, then it will load those files to Tassel
	// data tree.
	private void loadDataFileToTassel(int executeTimes) {
		File lopenFile;
		if (null != iPlugin.getImageJCacheFolder()) {
			String filePath = iPlugin.getImageJCacheFolder()
					+ IrodsUtilities.getPathSeperator()
					+ sourceIrodsFilePath.getName();
			File destinationLocalFilePath = new File(
					iPlugin.getImageJCacheFolder());
			iPlugin.getImageJCacheFolder();
			lopenFile = new File(destinationLocalFilePath.getAbsolutePath()
					+ IrodsUtilities.getPathSeperator()
					+ sourceIrodsFilePath.getName());
			iPlugin.getIRodsFileLoadPlugin().setOpenFiles(lopenFile);
			TasselPrefs.putOpenDir(iPlugin.getImageJCacheFolder());
			String myOpenFile = lopenFile.getPath();

			DataSet tds = null;
			try {

				if (iPlugin.getIRodsFileLoadPlugin().getFileType() == TasselFileType.Unknown) {
					if (myOpenFile.endsWith(FileLoadPlugin.FILE_EXT_HAPMAP)
							|| myOpenFile
									.endsWith(FileLoadPlugin.FILE_EXT_HAPMAP_GZ)) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.Hapmap);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.Hapmap);
					} else if ((myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_TOPM_H5))
							|| (myOpenFile
									.endsWith(FileLoadPlugin.FILE_EXT_TOPM))) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.TOPM);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.TOPM);
					} else if (myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_PLINK_PED)) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.Plink);
						String theMapFile = myOpenFile.replaceFirst(
								FileLoadPlugin.FILE_EXT_PLINK_PED,
								FileLoadPlugin.FILE_EXT_PLINK_MAP);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								theMapFile);
						iPlugin.getIRodsFileLoadPlugin().getPlinkLoadPlugin()
								.loadFile(myOpenFile, theMapFile, null);
					} else if (myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_PLINK_MAP)) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.Plink);
						String thePedFile = myOpenFile.replaceFirst(
								FileLoadPlugin.FILE_EXT_PLINK_MAP,
								FileLoadPlugin.FILE_EXT_PLINK_PED);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								thePedFile);
						iPlugin.getIRodsFileLoadPlugin().getPlinkLoadPlugin()
								.loadFile(thePedFile, myOpenFile, null);
					} else if (myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_SERIAL_GZ)) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.Serial);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.Serial);
					} else if (myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_HDF5)) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.HDF5);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.HDF5);
					} else if (myOpenFile.endsWith(FileLoadPlugin.FILE_EXT_VCF)
							|| myOpenFile.endsWith(FileLoadPlugin.FILE_EXT_VCF
									+ ".gz")) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.VCF);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.VCF);
					} else if (myOpenFile
							.endsWith(FileLoadPlugin.FILE_EXT_FASTA)
							|| myOpenFile
									.endsWith(FileLoadPlugin.FILE_EXT_FASTA
											+ ".gz")) {
						iPlugin.getIRodsFileLoadPlugin()
								.getLogger()
								.info("guessAtUnknowns: type: "
										+ TasselFileType.Fasta);
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
								myOpenFile, TasselFileType.Fasta);
					} else {
						iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
								myOpenFile);
						tds = iPlugin.getIRodsFileLoadPlugin().guessAtUnknowns(
								myOpenFile);
					}
				} else {
					iPlugin.getIRodsFileLoadPlugin().setAlreadyLoaded(
							myOpenFile);
					tds = iPlugin.getIRodsFileLoadPlugin().processDatum(
							myOpenFile,
							iPlugin.getIRodsFileLoadPlugin().getFileType());
				}

			} catch (Exception e) {
				e.printStackTrace();
				StringBuilder builder = new StringBuilder();
				builder.append("Error loading: ");
				builder.append(myOpenFile);
				builder.append("\n");
				builder.append(Utils.shortenStrLineLen(
						ExceptionUtils.getExceptionCauses(e), 50));
				String str = builder.toString();
				if (iPlugin.getIRodsFileLoadPlugin().isInteractive()) {
					DialogUtils.showError(str, iPlugin.getIRodsFileLoadPlugin()
							.getParentFrame());
				} else {
					iPlugin.getIRodsFileLoadPlugin().getLogger().error(str);
				}
			}

			if (tds != null) {
				iPlugin.getIRodsFileLoadPlugin().setResult(tds);
				iPlugin.getIRodsFileLoadPlugin().fireDataSetReturned(
						new PluginEvent(tds, FileLoadPlugin.class));
			}
		}
	}

	/***********************************************************************************************/
	@Override
	public void done() {
		/*
		 * Source code in done() method is shifted to openImageUsingImageJ()
		 * method
		 */
	}

	private void openImageUsingImageJ() {
		Opener imagejOpener = new Opener();
		ImagePlus imagePlusInstanceOfCurrentActiveImage = null;
		if (null != this.iPlugin.getImageJCacheFolder()) {
			String imageFilePath = this.iPlugin.getImageJCacheFolder()
					+ IrodsUtilities.getPathSeperator()
					+ this.sourceIrodsFilePath.getName();

			log.info("Current file opened by user: " + imageFilePath);
			ImagePlus imagePlus = imagejOpener.openImage(imageFilePath);
			if (null != imagePlus) {
				this.iPlugin.setImagePlus(imagePlus);
				log.info("ImagePlus instance is not null and before calling show() function of ImagePlus class");
				imagePlus.show();
				imagePlusInstanceOfCurrentActiveImage = IJ.getImage();

				log.info("ImagePlus instance of current image from IJ"
						+ imagePlusInstanceOfCurrentActiveImage);
				if (null != imagePlusInstanceOfCurrentActiveImage) {
					log.info("Current Image from IJ"
							+ imagePlusInstanceOfCurrentActiveImage.getImage());

					log.error("image windows are open");
					this.iPlugin.setImageOpened(true);
				} else {
					log.error("No image windows are open");
					this.iPlugin.setImageOpened(false);
				}
				log.info("irodsImagej.isImageOpened is set to true");
			} else {
				log.error("ImagePlus instance in GetFileFromIrodsSwingWorker is null and irodsImagej.isImageOpened is false");
				JOptionPane.showMessageDialog(null,
						"File format is not supported by ImageJ!", "Error", 0);
			}
		} else {
			IJ.showMessage("ImageJ is not able to open requested file!");
			IJ.showStatus("ImageJ is not able to open requested file!");
			log.error("ImagePlus instance is null and opening file Failed.");
			JOptionPane.showMessageDialog(null,
					"ImagePlus instance is null and opening file Failed!",
					"Error", 0);
		}
	}
}
